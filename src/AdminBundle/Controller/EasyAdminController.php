<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Advantage;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

//use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use AlterPHP\EasyAdminExtensionBundle\Controller\AdminController as BaseAdminController;

class EasyAdminController extends BaseAdminController
{

    public function persistEntity($entity)
    {
        $session = $this->get('session');
        $translator = $this->get('translator');
        $session->getFlashBag()->add('success', $translator->trans('crud.success.create'));
        
        parent::persistEntity($entity);
    }
    
    
    public function updateEntity($entity)
    {    
        $session = $this->get('session');
        $translator = $this->get('translator');
        $session->getFlashBag()->add('success', $translator->trans('crud.success.update'));
        
        parent::updateEntity($entity);
    }
    
    public function removeEntity($entity)
    {    
        $session = $this->get('session');
        $translator = $this->get('translator');
        $session->getFlashBag()->add('success', $translator->trans('crud.success.remove'));
        
        parent::removeEntity($entity);
    }
    
    /**
     * Surcharge des methodes users pour coller à FosUser
     */     
    public function createNewUserEntity()
    {
        return $this->get('fos_user.user_manager')->createUser();
    }

    public function persistUserEntity($entity)
    {
        $this->get('fos_user.user_manager')->updateUser($entity, false);
        $this->persistEntity($entity);
    }
    
    public function updateUserEntity($entity)
    {
        $this->checkForImage($entity);
        
        $this->get('fos_user.user_manager')->updateUser($entity, false);
        $this->updateEntity($entity);
    }
    
    /**
     * Vérifie si l'image de l'entité doit être supprimée ou pas lors de l'édition
     */
    private function checkForImage($entity)
    {
        if( $entity->getImage() && $entity->getImage()->getImage() == null && !$entity->getImage()->getImageFile())
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity->getImage());
            $entity->setImage(null);
            
            $em->flush();
        }
    }



    /**
     * Génération de la route à partir du nom de l'université
     */
    public function updateUniversityEntity($entity)
    {   
        
         $logo = $_FILES['university']['tmp_name']['logo']['imageFile']['file'];
        
         if(!empty($logo)){
            
            list($widthLogo, $heightLogo) = getimagesize($logo);

            
            if($heightLogo > 70){
                $session = $this->get('session');
                $session->getFlashBag()->add('error', "Logo : Hauteur maximale du logo dépassée (70 px)");
                
                header('Location: '.$_SERVER['REQUEST_URI']);
                die();
            }

         }
        

        $fullscreen = $_FILES['university']['tmp_name']['fullscreen']['imageFile']['file'];
            if(!empty($fullscreen)){
            
            list($widthFullscreen, $heightFullscreen) = getimagesize($fullscreen);

            if($widthFullscreen > 1920 or $heightFullscreen > 1020){
                $session = $this->get('session');
                $session->getFlashBag()->add('error', "Slide : Hauteur ou Largeur maximale dépassée (1920 x 1020 px)");
                
                header('Location: '.$_SERVER['REQUEST_URI']);
                die();
            }

        }
        



        $route = strtolower($entity->getName());
        $route = str_replace(' ', '-', $route);

        $entity->setRoute($route);


        // Bitly shortCode Api : generation du short link
        //$entity->setShortCode($this->shortCodeGeneretion($route));


        $this->persistEntity($entity);
    }

     public function persistUniversityEntity($entity)
    {   
       $logo = $_FILES['university']['tmp_name']['logo']['imageFile']['file'];
        
         if(!empty($logo)){
            
            list($widthLogo, $heightLogo) = getimagesize($logo);

            if($heightLogo > 70){
                $session = $this->get('session');
                $session->getFlashBag()->add('error', "Logo : Hauteur maximale du logo dépassée (70 px)");
                $entity->setLogo(null);
                //return;
            }
         }
        

          //$fullscreen = $_FILES['university']['tmp_name']['fullscreen']['imageFile']['file'];
         /*if(!empty($fullscreen)){
            
            $fullscreen = $_FILES['university']['tmp_name']['fullscreen']['imageFile']['file'];
            list($widthFullscreen, $heightFullscreen) = getimagesize($fullscreen);

            if($widthFullscreen > 1920 or $heightFullscreen > 1020){
                $session = $this->get('session');
                $session->getFlashBag()->add('error', "Slide : Hauteur ou Largeur maximale dépassée (1920 x 1020 px)");
                return;
            }

         }*/


        $route = strtolower($entity->getName());
        $route = str_replace(' ', '-', $route);

        $entity->setRoute($route);

        // Bitly shortCode Api : generation du short link
       // $entity->setShortCode($this->shortCodeGeneretion($route));
        
        $this->persistEntity($entity);
       
    }

    // Bitly shortCode Api : generation du short link
    /*public function shortCodeGeneretion($route){
        $bitly_client = $this->get('hpatoio_bitly.client');
        $shortLink = $bitly_client->Shorten(
            array(
                "longUrl" => $_SERVER['HTTP_ORIGIN']."/partenaires/".$route
                
            )
        );

        return $shortLink['url'];
    }*/
/*
    public function listUniversityAction(){
        $em = $this->getDoctrine()->getManager();

        $rpUniv = $em->getRepository('AdminBundle:University'); 
        $univs = $rpUniv->findAll(array());
        return $univs;

        //die("in");
    }*/


}
