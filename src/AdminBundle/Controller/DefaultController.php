<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {	
    	$locale = $request->getLocale();
    	return $this->redirectToRoute('easyadmin', array('_locale' => $locale));
        //return $this->render('@Admin/Default/index.html.twig');
    }
}
