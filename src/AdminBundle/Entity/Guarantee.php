<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Guarantee
 *
 * @ORM\Table(name="guarantee")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\GuaranteeRepository")
 */
class Guarantee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var text|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\Length(
     *      min = 34,
     *      minMessage = "Longeur minimale est de {{ limit }} characters"
     * )
     */
    private $description;


    /**
     * Many universities have Many guarantees.
     * @ORM\ManyToMany(targetEntity="University", mappedBy="guarantees", cascade={"persist"})
     * @ORM\JoinTable(name="universities_guarantees")
     */
    private $universities;

    
    public function __toString()
    {   
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Guarantee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Guarantee
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->universities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add university.
     *
     * @param \AdminBundle\Entity\University $university
     *
     * @return Guarantee
     */
    public function addUniversity(\AdminBundle\Entity\University $university)
    {
        $this->universities[] = $university;

        return $this;
    }

    /**
     * Remove university.
     *
     * @param \AdminBundle\Entity\University $university
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUniversity(\AdminBundle\Entity\University $university)
    {
        return $this->universities->removeElement($university);
    }

    /**
     * Get universities.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUniversities()
    {
        return $this->universities;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return Guarantee
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}
