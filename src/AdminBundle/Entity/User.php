<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist", "remove"})
     */
    private $image;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    
    public function __toString()
    {
        return $this->username;
    }
    

    /**
     * Set image.
     *
     * @param \AdminBundle\Entity\Image|null $image
     *
     * @return User
     */
    public function setImage(\AdminBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \AdminBundle\Entity\Image|null
     */
    public function getImage()
    {
        return $this->image;
    }
}
