<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;


/**
 * Advantage
 *
 * @ORM\Table(name="advantage")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\AdvantageRepository")
 */
class Advantage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * @Assert\Length(
     *      min = 0,
     *      max = 126,
     *      minMessage = "Longeur maximale est de {{ limit }} characters",
     *      maxMessage = "Longeur maximale est de {{ limit }} characters"
     * )
     */
    private $description;


    /**
    * @ORM\ManyToOne(targetEntity="University", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="university_id", referencedColumnName="id", onDelete="CASCADE")
    */
    private $university;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Advantage
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Advantage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
   
   
    /**
     * Set university.
     *
     * @param \AdminBundle\Entity\University|null $university
     *
     * @return Advantage
     */
    public function setUniversity(\AdminBundle\Entity\University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university.
     *
     * @return \AdminBundle\Entity\University|null
     */
    public function getUniversity()
    {
        return $this->university;
    }
}
