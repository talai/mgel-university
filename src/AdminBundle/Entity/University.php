<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * University
 *
 * @ORM\Table(name="university")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\UniversityRepository")
 */
class University
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;


    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    private $description;



    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string|null
     *
     * @ORM\Column(name="postal_code", type="string", length=10, nullable=true)
     */
    private $postalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="string", length=60, nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=30, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=true)
     */
    private $email;


    /**
     * @var string|null
     *
     * @ORM\Column(name="route", type="string", length=60, nullable=true, unique=true)
     */
    private $route;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shortCode", type="string", length=80, nullable=true)
     */
    private $shortCode;


    /**
     * @var string|null
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subtitleguarantee", type="string", length=255, nullable=true)
     */
    private $subtitleguarantee;


    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=20, nullable=true)
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist", "remove"})
     */
    private $logo;


    /**
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist", "remove"})
     */
    private $fullscreen;


    /**
     * Many universities have Many guarantees.
     * @ORM\ManyToMany(targetEntity="Guarantee", inversedBy="universities", cascade={"persist"})
     */
    private $guarantees;

    /**
     * @var string|null
     *
     * @ORM\Column(name="videoTitle", type="string", length=255, nullable=true)
     */
    private $videoTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="videoLink", type="text", nullable=true)
     */
    private $videoLink;


    public function __toString(){
        
        return $this->name;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return University
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }




    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return University
     */
    public function setAddress($address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return University
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return University
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set color.
     *
     * @param string|null $color
     *
     * @return University
     */
    public function setColor($color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string|null
     */
    public function getColor()
    {
        return $this->color;
    }

    
    /**
     * Set postalCode.
     *
     * @param string|null $postalCode
     *
     * @return University
     */
    public function setPostalCode($postalCode = null)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode.
     *
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set city.
     *
     * @param string|null $city
     *
     * @return University
     */
    public function setCity($city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->guarantees = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add guarantee.
     *
     * @param \AdminBundle\Entity\Guarantee $guarantee
     *
     * @return University
     */
    public function addGuarantee(\AdminBundle\Entity\Guarantee $guarantee)
    {
        $this->guarantees[] = $guarantee;

        return $this;
    }

    /**
     * Remove guarantee.
     *
     * @param \AdminBundle\Entity\Guarantee $guarantee
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGuarantee(\AdminBundle\Entity\Guarantee $guarantee)
    {
        return $this->guarantees->removeElement($guarantee);
    }

    /**
     * Get guarantees.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGuarantees()
    {
        return $this->guarantees;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return University
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    

    /**
     * Set logo.
     *
     * @param \AdminBundle\Entity\Image|null $logo
     *
     * @return University
     */
    public function setLogo(\AdminBundle\Entity\Image $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return \AdminBundle\Entity\Image|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set route.
     *
     * @param string|null $route
     *
     * @return University
     */
    public function setRoute($route = null)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route.
     *
     * @return string|null
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set subtitle.
     *
     * @param string|null $subtitle
     *
     * @return University
     */
    public function setSubtitle($subtitle = null)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle.
     *
     * @return string|null
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set fullscreen.
     *
     * @param \AdminBundle\Entity\Image|null $fullscreen
     *
     * @return University
     */
    public function setFullscreen(\AdminBundle\Entity\Image $fullscreen = null)
    {
        $this->fullscreen = $fullscreen;

        return $this;
    }

    /**
     * Get fullscreen.
     *
     * @return \AdminBundle\Entity\Image|null
     */
    public function getFullscreen()
    {
        return $this->fullscreen;
    }

   

    /**
     * Set subtitleguarantee.
     *
     * @param string|null $subtitleguarantee
     *
     * @return University
     */
    public function setSubtitleguarantee($subtitleguarantee = null)
    {
        $this->subtitleguarantee = $subtitleguarantee;

        return $this;
    }

    /**
     * Get subtitleguarantee.
     *
     * @return string|null
     */
    public function getSubtitleguarantee()
    {
        return $this->subtitleguarantee;
    }

    /**
     * Set videoTitle.
     *
     * @param string|null $videoTitle
     *
     * @return University
     */
    public function setVideoTitle($videoTitle = null)
    {
        $this->videoTitle = $videoTitle;

        return $this;
    }

    /**
     * Get videoTitle.
     *
     * @return string|null
     */
    public function getVideoTitle()
    {
        return $this->videoTitle;
    }

    /**
     * Set videoLink.
     *
     * @param string|null $videoLink
     *
     * @return University
     */
    public function setVideoLink($videoLink = null)
    {
        $this->videoLink = $videoLink;

        return $this;
    }

    /**
     * Get videoLink.
     *
     * @return string|null
     */
    public function getVideoLink()
    {
        return $this->videoLink;
    }


    

    /**
     * Set shortCode.
     *
     * @param string|null $shortCode
     *
     * @return University
     */
    public function setShortCode($shortCode = null)
    {
        $this->shortCode = $shortCode;

        return $this;
    }

    /**
     * Get shortCode.
     *
     * @return string|null
     */
    public function getShortCode()
    {
        return $this->shortCode;
    }

}
