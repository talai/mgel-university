function colorManage(){

	$('td.color').each(function(i, item){
		$(item).css("background-color", $(item).text()).css("color", "transparent");	
	})

}


(function ($) {
	
	$('#university_color').ColorPicker({ onChange: function (hsb, hex, rgb) {
		that = $("#university_color");
		that.val('#'+hex);
		that.css("color", "transparent");
		that.css("background-color", '#'+hex);

	} });

	$('#university_color').css("background-color", $('#university_color').val()).css("color", "transparent");

	colorManage();

})(jQuery)
