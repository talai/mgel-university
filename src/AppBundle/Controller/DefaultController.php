<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\{TextType, EmailType, TextareaType};

use AdminBundle\Entity\Advantage;

class DefaultController extends Controller
{
    public function indexAction(Request $request, \Swift_Mailer $mailer)
    {   

        $em = $this->getDoctrine()->getManager();
        $rpUniv = $em->getRepository('AdminBundle:University'); 
         $univs = $rpUniv->findAll(array());

        return $this->render('@App/Default/index.html.twig', [
            'univs' => $univs
        ]);
    }
    
    public function univAction($route)
    {   
        $em = $this->getDoctrine()->getManager();
        $rpUniv = $em->getRepository('AdminBundle:University');
        $univ = $rpUniv->findOneBy(array('route' => $route));

        if($univ){
            $advs = $this->getDoctrine()
                ->getRepository(Advantage::class)
                ->getAdvByUnivId($univ->getId());

            return $this->render('@App/Default/univ.html.twig', [ 'univ' => $univ, 'advs' => $advs]);

        }else{
            //@@ todo prévoir de rediriger vers un template 404, le cas de route introuvable
            die("404");
        }
        
    }
    
}
